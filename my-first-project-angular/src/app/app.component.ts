import {Component, OnInit} from '@angular/core';
import {Apparate, AppareilService} from "./services/appareil.service";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  isAuth = false;
  apparate: Apparate[] = [];

  constructor(private appareilService: AppareilService) {
    setTimeout(
      () => {
        this.isAuth = true;
      }, 4000
    );

  }

  ngOnInit() {
    this.appareilService.getApparates().subscribe(data => {
      this.apparate = data;
      console.log(data);
    });
  }

}
