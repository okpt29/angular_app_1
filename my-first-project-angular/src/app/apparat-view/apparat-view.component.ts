import {Component, Injectable, Input, OnInit} from '@angular/core';
import {Apparate, AppareilService} from "../services/appareil.service";
@Component({
  selector: 'app-apparat-view',
  templateUrl: './apparat-view.component.html',
  styleUrls: ['./apparat-view.component.scss']
})
export class ApparatViewComponent implements OnInit {

  apparate: Apparate[] = [];
  isAuth = false;

  constructor(private appareilService: AppareilService) {
    setTimeout(
      () => {
        this.isAuth = true;
      }, 4000
    );
  }

  ngOnInit(){
    this.appareilService.getApparates().subscribe( data =>{
      this.apparate = data;
      console.log(data);
    });
  }

  onTurnOn(index:number) {
    this.appareilService.switchOnAll(index);
  }

  // @ts-ignore
  onTurnDown(index: number) {
    if (confirm('sind Sie sicher das Apparat ausmachen zu wollen ?')){
      // @ts-ignore
      this.appareilService.switchOffAll(index);
    } else {
      return null;
    }
  }

}
