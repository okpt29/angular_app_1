import {Observable, of} from "rxjs";
import {Injectable} from "@angular/core";

export interface Apparate {
  name: string;
  status: string;
  content: string;
  love: number;
  disLove: number;
  date: Date;
  index: number;
}

@Injectable({
  providedIn: 'root'
})
export class AppareilService {

  private data: Apparate[] = [
    {
      name: 'Wachmachine',
      status: 'aus',
      content: "Wachmachine von LG gebaut mit sehr viele umfangreiche Funktionen, mit 3 jahre Garantie",
      love: 0,
      disLove: 0,
      date: new Date(),
      index: 1
    },
    {
      name: 'Kühlschank',
      status: 'an',
      content: "Tolle und Moderne Kühlschank mit Glasfach, 3 Jahre Garantie !" +
        "von Concept gebaut ",
      love: 0,
      disLove: 0,
      date: new Date(),
      index: 2
    },
    {
      name: 'Computer',
      status: 'aus',
      content: "Gaming Laptop sehr Robust von Samsung , mit 2 Jahre Garantie",
      love: 0,
      disLove: 0,
      date: new Date(),
      index: 3
    }
  ];

  getApparates(): Observable<Apparate[]> {
    return of(this.data);
  }

  switchOnAll(index: number) {
    const element = this.data.find(elem => elem.index === index);
    if (element)
      element.status = 'an';
  }

  switchOffAll(index: number) {
    const element = this.data.find(elem => elem.index === index);
    if (element)
      element.status = 'aus';
  }
}
