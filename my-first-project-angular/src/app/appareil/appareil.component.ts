import {Component, Input, OnInit} from '@angular/core';
import {Apparate, AppareilService} from "../services/appareil.service";
import {MatDialog} from "@angular/material/dialog";
import {ConfirmComponent} from "../confirm/confirm.component";

@Component({
  selector: 'app-appareil',
  templateUrl: './appareil.component.html',
  styleUrls: ['./appareil.component.scss']
})
export class AppareilComponent implements OnInit {

  lastUpdate = new Date();

  @Input() apparat?: Apparate;
  isLike = false;
  isUnlike = false;

  constructor(private appareilService: AppareilService,
              private dialog: MatDialog) {
  }

  ngOnInit() {
  }

  getStatus() {
    return this.apparat?.status;
  }

  getColor() {
    if (this.apparat?.status == "an") {
      return 'green';
    }
    return 'red';
  }

  onAnschalt1() {
    this.isLike = !this.isLike;
    if (this.isLike)
      this.isUnlike = false;
    console.log("es wird gelikt")
    if (this.apparat)
      this.apparat.love++;
  }

  onAnschalt2() {
    this.isUnlike = !this.isUnlike;
    if (this.isUnlike)
      this.isLike = false;
    console.log("es wird dislikt")
    if (this.apparat)
      this.apparat.disLove++;
  }

  //Apparat Schaltungen
  onSwitch() {
    if (this.apparat?.status === 'aus') {
      this.appareilService.switchOnAll(this.apparat?.index);
    } else if (this.apparat?.status == 'an') {
      const ref = this.dialog.open(ConfirmComponent, {
        maxWidth: "400px",
        data: {
          title: 'Bist du sicher?',
          message: 'Du kannst nichts mehr ändern.'
        }
      });
      const app = this.apparat;
      ref.afterClosed().subscribe(res => {
        if (res) {
          this.appareilService.switchOffAll(app?.index);
        }
      });
    }
  }
}
