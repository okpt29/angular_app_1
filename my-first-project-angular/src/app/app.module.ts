import {NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';

import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {MyFirstComponent} from './my-first/my-first.component';
import {AppareilComponent} from './appareil/appareil.component';
import {FormsModule} from "@angular/forms";
import {AppareilService} from "./services/appareil.service";
import {ConfirmComponent} from './confirm/confirm.component';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {MatCardModule} from "@angular/material/card";
import {MatButtonModule} from "@angular/material/button";
import {MatDialogModule} from "@angular/material/dialog";
import { AuthComponent } from './auth/auth.component';
import { ApparatViewComponent } from './apparat-view/apparat-view.component';
import {RouterModule, Routes} from "@angular/router";

const appRoutes: Routes = [
  { path: `apparate`, component: AppareilComponent },
  { path: `auth`, component: AuthComponent},
  { path: ``, component: ApparatViewComponent}
];

@NgModule({
  declarations: [
    AppComponent,
    MyFirstComponent,
    AppareilComponent,
    ConfirmComponent,
    AuthComponent,
    ApparatViewComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    BrowserAnimationsModule,
    MatCardModule,
    MatButtonModule,
    MatDialogModule,
    RouterModule.forRoot(appRoutes)
  ],
  providers: [
    AppareilService
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
}
