import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ApparatViewComponent } from './apparat-view.component';

describe('ApparatViewComponent', () => {
  let component: ApparatViewComponent;
  let fixture: ComponentFixture<ApparatViewComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ApparatViewComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ApparatViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
